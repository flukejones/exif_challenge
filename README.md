# ExifIt

This is a small utility to process files to gather select EXIF and file metadata
and then write this as a pretty-formatted JSON file alongside the input files.

Multiple files can be specified for input.

The utility makes every effort not to fail, and will continue to process all
input files while logging any errors or warnings. On completion it will notify
the user of which inputs failed.

## Running

`cargo run <input file paths>`