use log::{error, warn, info, LevelFilter};
use std::env;
use std::io::Write;

fn main() {
    let mut logger = env_logger::Builder::new();
    logger
        // TODO: in a production environment this would output to stderr or a log
        .target(env_logger::Target::Stdout)
        .format(|buf, record| writeln!(buf, "{}: {}", record.level(), record.args()))
        .filter(None, LevelFilter::Info)
        .init();

    let args = env::args();

    if args.len() == 1 {
        error!("No input supplied");
    }

    let mut failed = Vec::new();
    for arg in args.skip(1) {
        match exifit::ImageData::new(&arg).and_then(|file| file.read_exif()) {
            Ok(data) => {
                data.write_json()
                    .map_err(|err| {
                        error!("File write: {}", err);
                    })
                    .unwrap_or(());
            }
            Err(err) => {
                error!("{}: {}", arg, err);
                failed.push(arg);
            }
        }
    }
    if failed.is_empty() {
        info!("All input files parsed successfully");
    } else {
        for fail in failed {
            warn!("Failed to process {}", fail);
        }
    }
}
