use chrono::{DateTime, SecondsFormat, Utc};
use log::warn;
use serde::{Serialize};
use std::path::{Path, PathBuf};

/// The data structure an input image file will be parsed to. On creation the
/// EXIF data is not filled in, the output is expected to be chained with
/// `read_exif` as a 'builder' style pattern.
///
/// # Example
/// ```ignore
/// let path = "images/JAM26496.jpg";
/// let exif = exifit::ImageData::new(&path)?.read_exif()?;
/// ```
#[derive(Serialize, Debug)]
pub struct ImageData {
    filename: String,
    // TODO: custom serialize to output <n>KiB or scale up value depending
    //  on how large the number is, e.g, <n>MiB, <n>GiB
    size: u64,

    // Leave these fields visible to alert users of potential issues
    created_time: Option<String>,
    modified_time: Option<String>,

    // Do not output empty EXIF tags
    #[serde(skip_serializing_if = "is_none")]
    orientation: Option<u32>,
    #[serde(skip_serializing_if = "is_none")]
    capture_time: Option<String>,
    #[serde(skip_serializing_if = "is_none")]
    camera_model: Option<String>,
    // A camera serial may start with one or more zeroes, so must be a string
    #[serde(skip_serializing_if = "is_none")]
    camera_serial: Option<String>,
    
    // Do not show this at all
    #[serde(skip_serializing)]
    file_path: PathBuf,
    // kamadak-exif is unable to use jpg data in a buffer like this as it
    // appears to be read in an unexpected order?
    //file_buffer: Option<Vec<u8>>,
}

impl ImageData {
    /// Create initial ImageData without exif data filled in, to be chained with
    /// `read_exif`
    pub fn new(path: &str) -> Result<Self, anyhow::Error> {
        let path = Path::new(path).canonicalize()?;
        let meta = std::fs::metadata(&path)?;

        if meta.is_dir() {
            return Err(anyhow::anyhow!("Path is not a file"));
        }

        // Because the majority of errors in this function will be IO based,
        // we'll convert these None to IOError. Our struct isn't much use
        // without the file basics.
        let filename = path
            .file_name()
            .ok_or_else(|| anyhow::anyhow!("Invalid filename"))?
            .to_str()
            .ok_or_else(|| anyhow::anyhow!("Invalid filename"))?
            .to_string();

        // We're not returning errors here as this shouldn't affect basic functionality
        // but we will log any issues so the user is aware
        let created_time = meta.created().map_or_else(
            |err| {
                warn!("created_time: {}", err);
                None
            },
            |time| Some(DateTime::<Utc>::from(time).to_rfc3339_opts(SecondsFormat::Secs, true)),
        );

        let modified_time = meta.created().map_or_else(
            |err| {
                warn!("modified_time: {}", err);
                None
            },
            |time| Some(DateTime::<Utc>::from(time).to_rfc3339_opts(SecondsFormat::Secs, true)),
        );

        Ok(ImageData {
            filename,
            size: meta.len(),
            created_time,
            modified_time,
            //
            orientation: None,
            capture_time: None,
            camera_model: None,
            camera_serial: None,
            //
            file_path: path,
        })
    }

    /// Fetch all relevant EXIF data from the requested file
    pub fn read_exif(mut self) -> Result<Self, anyhow::Error> {
        let exifreader = exif::Reader::new();
        // This read_raw function really should be taking an &[u8] to
        // prevent consuming the data in memory or requiring a clone()
        //let exif = exifreader.read_raw(data)?;

        let file = std::fs::File::open(&self.file_path)?;
        let mut bufreader = std::io::BufReader::new(&file);
        let exif = exifreader.read_from_container(&mut bufreader)?;

        // A useful secondary reference to exif crate source is
        // https://www.vcode.no/web/resource.nsf/II2LNUG/642.htm
        for f in exif.fields() {
            // Model
            if f.tag == exif::Tag::Model {
                let value = f.display_value().to_string();
                // remove String quotes
                self.camera_model = Some(value[1..value.len() - 1].to_string());
            }
            // Body Serial
            if f.tag == exif::Tag::BodySerialNumber {
                let value = f.display_value().to_string();
                // remove String quotes
                self.camera_serial = Some(value[1..value.len() - 1].to_string());
            }
            // Orientation
            if f.tag == exif::Tag::Orientation {
                self.orientation = f.value.get_uint(0);
            }
            // Original capture date
            if f.tag == exif::Tag::DateTimeOriginal {
                let tmp = f.display_value().to_string() + "+0000";
                // We need to parse from "2020-01-30 09:44:56", we're also going to assume UTC
                let parsed = DateTime::parse_from_str(&tmp, "%Y-%m-%d %H:%M:%S %z")?;
                let time = parsed.to_rfc3339_opts(SecondsFormat::Secs, true);
                self.capture_time = Some(time);
            }
        }
        Ok(self)
    }

    /// Writes all data to a pretty-formatted JSON file alongside the original
    /// input file.
    pub fn write_json(&self) -> Result<(), anyhow::Error> {
        let buf = serde_json::to_string_pretty(&self)?;
        let path = self.file_path.with_extension("json");
        std::fs::write(path, buf)?;
        Ok(())
    }
}

fn is_none<T>(s: &Option<T>) -> bool {
    s.is_none()
}

mod test {
    use crate::ImageData;

    #[test]
    fn parse_jam26496() {
        let path = "images/JAM26496.jpg";
        let data = ImageData::new(path);
        assert!(data.is_ok());
        let data = data.unwrap();

        assert_eq!(data.filename, "JAM26496.jpg");
        assert_eq!(data.size, 353914);
        assert!(data.camera_model.is_none());

        let data = data.read_exif();
        assert!(data.is_ok());
        let data = data.unwrap();

        assert_eq!(data.camera_model, Some("Canon EOS 5D Mark IV".to_string()));
        assert_eq!(data.camera_serial, Some("025021000535".to_string()));
        assert_eq!(data.capture_time, Some("2020-01-30T09:44:56Z".to_string()));
    }

    #[test]
    fn parse_filename_error() {
        let path = "images/JAM26496";
        let data = ImageData::new(path);
        assert!(data.is_err());
    }

    #[test]
    fn parse_dir_error() {
        let path = "images";
        let data = ImageData::new(path);
        assert!(data.is_err());
    }

    #[test]
    fn parse_exif_error() {
        let path = "src/main.rs";
        let data = ImageData::new(path);
        assert!(data.is_ok());

        let e = data.unwrap().read_exif();
        assert!(e.is_err());
    }
}
